﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour {

    private Animator anim;
    public bool isTorchOn;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }

        private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "fire")
        {
            isTorchOn = true;
            anim.SetBool("fire", true);
        }
    }
}
