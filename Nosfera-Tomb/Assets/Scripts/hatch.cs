﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hatch : MonoBehaviour {

    bool isOpen = false;


	void Update () {
        isOpen = GameObject.Find("Torch_Activate").GetComponent<Torch>().isTorchOn;
        if (isOpen)
        {
            gameObject.SetActive(false);
        }
    }
}
