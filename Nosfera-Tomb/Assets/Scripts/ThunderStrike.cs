﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderStrike : MonoBehaviour {

    public int damage = 20;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerHealth player = collision.collider.GetComponent<PlayerHealth>();
            player.TakeDamage(damage);
        }
    }
}
