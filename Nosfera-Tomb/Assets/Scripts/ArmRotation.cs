﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {

    public int rotationOffset = 0;
    public int rotationMin = 0;
    public int rotationMax = 90;
    public int restingOffset = -90;
    bool isFlipped = false;
    bool isUpsideDown = false;
    bool facingRight = true;			
    bool upsideDown = false;
    public float h;

    int spriteFlip = 1;



    // Update is called once per frame
    void Update () {

		Vector3 difference = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position ;
        difference.Normalize ();

        isFlipped = !GameObject.Find("Player").GetComponent<PlayerControl>().facingRight;
        isUpsideDown = GameObject.Find("Player").GetComponent<PlayerControl>().upsideDown;

        restingOffset = isFlipped ? 90 : -90;
        rotationOffset = isFlipped ? 180 : 0;

        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        if (Input.GetMouseButton(0))
        {
            transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f + restingOffset);
        }



        if (isUpsideDown)
        {
            UpsideDown();
        }
    }

    void UpsideDown()
    {
        upsideDown = !upsideDown;
        Vector3 theScale = transform.localScale;
        theScale.y *= -1;
        transform.localScale = theScale;
    }
}
