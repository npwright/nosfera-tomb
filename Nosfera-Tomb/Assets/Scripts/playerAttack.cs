﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAttack : MonoBehaviour {

    public GameObject projectile;
    public float fireRate = 0;
    bool canShoot = true;
    public float cooldown = 3f;

    public Transform firePoint;
    GameObject clone;
    public float speed = 6;
    private Animator anim;		

    void Update()
    {
        anim = GetComponentInChildren<Animator>();
        if (canShoot)
        {
            if(Input.GetMouseButtonDown(1) && Input.GetMouseButton(0))
            {
                Shoot();
            }
        }
    }

    IEnumerator CanShoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(cooldown);
        canShoot = true;
    }

    void Shoot()
    {
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        Vector2 direction = (mousePosition - firePointPosition);
        direction.Normalize();
        clone = Instantiate(projectile, firePoint.position, firePoint.rotation);
        clone.GetComponent<Rigidbody2D>().velocity = direction * speed;
        StartCoroutine(CanShoot());
    }
}


