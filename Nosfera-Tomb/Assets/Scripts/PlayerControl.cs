﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
    public bool upsideDown = false;         // For determining whether the player is upside down.
	public bool jump = false;				// Condition for whether the player can jump.
    public bool gravity = false;            // Condition for whether the player can switch gravity.
    public float h;

	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.

	public Transform groundCheck;			// A position marking where to check if the player is grounded.
	public bool floorGrounded = false;			// Whether or not the player is grounded on the floor.
    SpriteRenderer spriteRenderer;
    Rigidbody2D rb2d;

	private Animator anim;					// Reference to the player's animator component.

    


	void Awake()
	{
		groundCheck = transform.Find("groundCheck");
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}


	void Update()
	{
        anim.SetBool("ground", floorGrounded);
		// The player is floorgrounded if a linecast to the groundcheck position hits anything on the ground layer.
		floorGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		// If the jump button is pressed and the player is grounded then the player should jump.
		if(Input.GetButtonDown("Jump") && floorGrounded)
			jump = true;

        if (Input.GetKeyDown(KeyCode.Q) && !floorGrounded)
            gravity = true;
	}


	void FixedUpdate ()
	{
		float h = Input.GetAxis("Horizontal");      // Cache the horizontal input.

        // The Speed animator parameter is set to the absolute value of the horizontal input.
        anim.SetFloat("Speed", Mathf.Abs(h));

        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
			// ... add a force to the player.
			GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

		// If the input is moving the player right and the player is facing left...
		if(h > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h < 0 && facingRight)
			// ... flip the player.
			Flip();

		if(jump)        // Jumps the player if they are able.
        {
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));			// Add a vertical force to the player.
			jump = false;
		}

        if (gravity)
        {
            jump = true;
            Gravity();
        }

	}
	
	void Flip ()        // Switch the way the player is labelled as facing.
    {
		facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
	}

    void Gravity()      // Switched the way the player's up is, flips gravityScale, and flips jumpForce.
    {
        rb2d.gravityScale *= -1;
        upsideDown = !upsideDown;
        Vector3 theScale = transform.localScale;
        theScale.y *= -1;
        transform.localScale = theScale;
        jumpForce *= -1;
        gravity = false;
        jump = false;
    }
}
