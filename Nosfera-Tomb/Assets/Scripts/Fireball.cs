﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

    int damage = 10;
    private Animator anim;
    bool canDestroy = false;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {
            Enemy enemy = collision.collider.GetComponent<Enemy>();
            enemy.DamageEnemy(damage);
            anim.SetBool("onHit", true);
            Debug.Log("Boom");
            StartCoroutine(DestroyWait());
            if (canDestroy)
            {
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.tag != "Player")
        {
            anim.SetBool("onHit", true);
            Debug.Log("Splat");
            StartCoroutine(DestroyWait());
            if (canDestroy)
            {
                Destroy(gameObject);
            }


        }

    }

    IEnumerator DestroyWait()
    {
        yield return new WaitForSeconds(1);
        canDestroy = true;

    }
}

