﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int currentHealth;
    // public Image healthFill;
   //  public Image damageImage;
    public float flashSpeed = 5f;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);

    public bool isDead;
    bool damaged;
    private Animator anim;	


    private void Awake()
    {
        anim = GetComponent<Animator>();
        currentHealth = startingHealth;
        health();
    }


	// Update is called once per frame
	void Update () {
       // health();
		
	}

    public void TakeDamage (int amount)
    {
        damaged = true;
        Debug.Log("Ouch");
        if(currentHealth != 0)
        {
            currentHealth -= amount;
        }
        if(currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }


    void Death()
    {
        isDead = true;
        anim.SetBool("isDead", isDead);
        StartCoroutine(DeathCount());

    }

    void health()
    {
      //  healthFill.fillAmount = currentHealth / 100f;
    }

    IEnumerator DeathCount()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
