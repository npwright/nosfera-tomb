﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int maxHealth = 100;
    public int damage = 20;
    private int _curHealth;
    public int curHealth
        {
            get { return _curHealth;  }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth);  }
        }
    public Vector2 pos1 = new Vector2(0,-4);
    public Vector2 pos2 = new Vector2(0,4);
    public float speed = 5;
    public bool canVAttack;
    public bool canHAttack;
    public GameObject thunderStrikePrefab;

    public Transform thunderStrikeLoc1;
    public Transform thunderStrikeLoc2;
    public Transform thunderStrikeLoc3;
    Transform thunderStrikeSpawnLocation;
    int location = 1;
    int count = 0;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    void Awake () {
        Init();
        VerticleAttack();
        //HorizontalAttack();

    }

    private void FixedUpdate()
    {
        Movement();
    }

    public void DamageEnemy (int damage)
    {
        curHealth -= damage;
        if (curHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void Init()
    {
        curHealth = maxHealth;
    }

    void Movement()
    {
        transform.position = Vector2.Lerp(pos1, pos2, Mathf.PingPong(Time.time * speed, 1.0f));
    }

    void VerticleAttack()
    {
        VerticleSpawnCount();
        ThunderStrike();
        Debug.Log("Vertical Attack");
        StartCoroutine(Verticle_Attack());
    }

    private void ThunderStrike()
    {
        Instantiate(thunderStrikePrefab, thunderStrikeSpawnLocation.position, Quaternion.identity);
    }

    void VerticleSpawnCount()
    {
        if (location == 1)
        {
            thunderStrikeSpawnLocation = thunderStrikeLoc1;
            location += 1;
        }
        else if (location == 2)
        {
            thunderStrikeSpawnLocation = thunderStrikeLoc2;
            location += 1;
        }
        else if (location == 3)
        {
            thunderStrikeSpawnLocation = thunderStrikeLoc3;
            location = 1;
        }
    }
    /*
    void HorizontalAttack()
    {
        Debug.Log("Horizontal Attack");
        StartCoroutine(Horizontal_Attack());
    }
    */
    IEnumerator Verticle_Attack()
    {
        yield return new WaitForSeconds(5);
        VerticleAttack();
    }
    /*
    IEnumerator Horizontal_Attack()
    {
        yield return new WaitForSeconds(15);
        HorizontalAttack();
    }
    */
}
