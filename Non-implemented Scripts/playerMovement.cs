﻿using System;
using UnityEngine;

public class playerMovement : MonoBehavior
{
    private bool onFloor = true;        //if character is walking on floor
    private bool onCeiling = false;     //if character is walking on ceiling

    public float acceleration = 10f;    //character acceleration when moving
    public float maxSpeed = 5f;         //character top speed
    public float jumpForce = 100f;      //the power that the character can jump with

    private Transform groundCheck;      //a check to see if the character is grounded or in midair
    private bool grounded = false;      //if the character is grounded
    private bool facingRight = true;    //if the character is facing right

    private RigidBody2D rb2d;

    void Awake()
    {
        groundCheck = transform.Find("groundCheck");
        rb2d = GetComponent<Rigidbody2D>();
    }





    private void Flip()
    {
        facingRight = !facingRight;
        transform.localScale.x *= -1;
    }
}
